mongoimport --db test --collection spells --type json --file /dnd_data/spells.json --jsonArray
mongoimport --db test --collection monsters --type json --file /dnd_data/monsters.json --jsonArray
mongoimport --db test --collection playlists --type json --file /dnd_data/playlists.json --jsonArray
mongoimport --db test --collection backgrounds --type json --file /dnd_data/backgrounds.json --jsonArray
mongoimport --db test --collection conditions --type json --file /dnd_data/conditions.json --jsonArray
mongoimport --db test --collection feats --type json --file /dnd_data/feats.json --jsonArray
mongoimport --db test --collection items --type json --file /dnd_data/items.json --jsonArray
mongoimport --db test --collection races --type json --file /dnd_data/races.json --jsonArray
