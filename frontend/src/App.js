import React from 'react';

import Menu from './Components/Menu';
import {defaultMenuPlaylistIframe} from './Components/Playlists/SinglePlaylist'
import {SpellBook} from "./Components/SpellBook/SpellBook";

export var SERVER_URL = "http://127.0.0.1:5000/";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeWindow: SpellBook,
            activePlaylistIframe: defaultMenuPlaylistIframe(),
        };
    }

    render() {
        return (
            <div>
                <this.state.activeWindow App={this}/>
                <Menu App={this} activePlaylistIframe={this.state.activePlaylistIframe}/>
            </div>
        );
    }

}

export default App;