import React from 'react';
import {SpellBook} from "./SpellBook/SpellBook";
import PlaylistGrid from "./Playlists/PlaylistGrid";
import ImageDisplayer from "./Displayer/ImageDisplayer";
import MonstersGrid from "./Monsters/MonstersGrid";

const MenuStyle = {
    textAlign: "center",
    position: "fixed",
    margin: "auto",
    fontSize: "30px",
    bottom: 0,
    backgroundColor: "#111111",
    paddingTop: "100px",
    width: "90px",
    top: 0,
};

const menuItemStyle = {
    height: "65px",
    margin: window.innerWidth < 500 ? "3px" : "auto",
    fontSize: window.innerWidth < 500 ? "12px" : "30px",
    width: window.innerWidth < 500 ? "20%" : "65px",
    display: window.innerWidth < 500 ? "inline-block" : "block",
    padding: window.innerWidth < 500 ? "" : "10px",
    marginTop: window.innerWidth < 500 ? "" : "15px",
};


class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mobile: window.innerWidth < 500,
        };

        this.buttons = [
            <img style={menuItemStyle} id="menu-spells" key="spells" src={require('../img/Icons/spells_icon.svg')}
                 alt={'spellbook'} onClick={() => this.menuSpellsClicked()}/>,

            <img style={menuItemStyle} id="menu-monsters" key="monsters" src={require('../img/Icons/monster_icon.svg')}
                 alt={'monsters'} onClick={() => this.menuMonstersClicked()}/>,

            <img style={menuItemStyle} id="menu-playlists" key="playlists"
                 src={require('../img/Icons/music_icon.svg')} alt={'plylist'}
                 onClick={() => this.menuPlaylistClicked()}/>,

            <img style={menuItemStyle} id="menu-displayer" key="displayer"
                 src={require('../img/Icons/fight_icon.svg')} alt={'displayer'}
                 onClick={() => this.menuDisplayerClicked()}/>,

        ];
    }


    render() {
        return (
            <div style={MenuStyle}>
                {this.buttons}

                {this.state.mobile ? "" : this.props.activePlaylistIframe}
            </div>
        );
    }

    menuSpellsClicked = () => {
        this.props.App.setState({activeWindow: SpellBook});

    };

    menuPlaylistClicked = () => {
        this.props.App.setState({activeWindow: PlaylistGrid});
    };

    menuDisplayerClicked = () => {
        this.props.App.setState({activeWindow: ImageDisplayer});
    };


    menuMonstersClicked = () => {
        this.props.App.setState({activeWindow: MonstersGrid});
    };

}

export default Menu;