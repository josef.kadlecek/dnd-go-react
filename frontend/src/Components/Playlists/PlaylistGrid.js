import React from 'react';
import {SERVER_URL} from "../../App";
import PlaylistCategory from "./PlaylistCategory";

const PlaylistGridStyle = {
    marginTop: "100px",
    textAlign: "center",
    display: "inline",
    paddingLeft : "90px",
    width:"80%"

};


class PlaylistGrid extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            allPlaylists: [],
        }
    }

    render() {
        return (

            <div style={PlaylistGridStyle}>
                {
                    Object.keys(this.state.allPlaylists).map((playlistCategory, i) =>
                        <PlaylistCategory category={this.state.allPlaylists[playlistCategory]} App={this.props.App}
                                          key={i}/>
                    )
                }
            </div>

        );
    }

    componentDidMount() {
        //Get playlist list
        fetch(SERVER_URL + "playlists", {
            mode: "cors",
            headers: {"Content-Type": "application/json"},
        })
            .then(response => response.json())
            .then(response => {
                this.setState({allPlaylists: response});
            })
            .catch(err => console.error(err));

    }

}

export default PlaylistGrid;