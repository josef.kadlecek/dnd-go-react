import React from "react";

const spotifyPlayerMenuStyle = {
    top: 20,
    left: 5,
    position: "absolute",
    border: "0px",
    height: "80px",
    width: "80px",
    margin : "auto",
};

export function defaultMenuPlaylistIframe() {
    return <div>
        <iframe allow="encrypted-media" title={"menu_palyer"} key={"menu_player"}
                style={spotifyPlayerMenuStyle} src={"https://open.spotify.com/embed/playlist/5r2AkNQOITXRqVWqYj40QG?"}/>
    </div>
}


class SinglePlaylist extends React.Component {
    constructor(props) {
        super(props);
        this._isMounted = false;
        this.state = {
            imgUrl: ""
        };
    }


    playlistSelectionChanged() {
        this.props.App.setState({activePlaylistIframe: this.render_small()})
    }


    render_small() {

        return <div>
            <iframe allow="encrypted-media" title={this.props.src} key={this.props.src}
                    style={spotifyPlayerMenuStyle} src={this.props.playlist_url}/>
        </div>
    }

    render() {
        return (
                <img id={this.props.img_url} key={this.props.img_url} src={this.props.img_url} width={"250px"}
                     alt={this.props.img_url} onClick={() => this.playlistSelectionChanged()}/>
        )

    }


}

export default SinglePlaylist;