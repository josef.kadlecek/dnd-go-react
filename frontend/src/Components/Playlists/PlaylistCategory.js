import React from 'react';
import SinglePlaylist from "./SinglePlaylist";

const SingleCategoryStyle = {
    paddingLeft : "90px",
    margin : "5px",
};


class PlaylistCategory extends React.Component {


    render() {
        return (
            <div style={SingleCategoryStyle}>
                {
                    this.props.category.playlists.map((playlist, i) =>
                        <SinglePlaylist img_url={playlist.img_url} playlist_url={playlist.playlist_url}
                                        key={i} App={this.props.App}/>
                    )
                }
            </div>
        )
    }
}

export default PlaylistCategory;