import React from "react";

import {ClassToken} from "./ClassToken"
import {SERVER_URL} from "../../App";


const ClassTokenBarStyle = {
    margin: "auto",
    textAlign: "center",
    background: "#F7CE65",
    border : "solid #930C10",
    borderBottom : "0px"
};

//TODO: Aditional filtering options: text, level....
export class ClassTokenBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            all_classes: [],
        };
    }

    render() {
        return (
            <div style={ClassTokenBarStyle}>
                {/*Create token for every class*/}
                {this.state.all_classes.map((cls, i) => <ClassToken ClassTokenBar={this} key={i} class_name={cls}/>)}
            </div>

        );
    }

    //Fetch existing classes
    componentDidMount() {
        fetch(SERVER_URL + "classes", {
            mode: "cors",
            headers: {"Content-Type": "application/json"},
        })
            .then(response => response.json())
            .then(response => {
                this.setState({all_classes: response});
            })
            .catch(err => console.error(err));
    }

    removeFromArray(arr, value) {
        return arr.filter(function (ele) {
            return ele !== value;
        });

    }

    selectionChanged(class_name, selected) {
        let Selected_classes = this.props.SpellBook.state.selected_classes;

        if (selected === true) {
            Selected_classes.push(class_name)
        } else {
            Selected_classes = this.removeFromArray(Selected_classes, class_name);
        }
        this.props.SpellBook.setState({selected_classes: Selected_classes});
    }
}