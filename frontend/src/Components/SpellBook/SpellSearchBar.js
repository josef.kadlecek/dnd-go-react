import React from 'react';


const SpellInputStyle = {
    margin: "auto",
    borderColor: "black",
    width: "80%",
    height: "40px",
    fontSize: "120%",
    border: "solid #930C10",
};

const SpellSearchFormStyle = {
    margin: "auto",
    textAlign: "center",
    background: "#F7CE65",
    border: "solid #930C10",
    borderTop: "0px",
    padding: "10px"
};


export class SpellSearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchBarString: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        this.setState({
            searchBarString: event.target.value
        });
        this.props.SpellBook.setState({filter: event.target.value});
        event.preventDefault();
    }

    render() {
        return (
            <form style={SpellSearchFormStyle}>
                <input style={SpellInputStyle} type={"text"} name="searchStringInput" autoComplete="off"
                       value={this.state.searchBarString} onChange={this.handleInputChange}
                       onKeyPress={(e) => {
                           e.key === 'Enter' && e.preventDefault();
                       }}/>
            </form>
        );
    }
}
