import React from 'react';
import parse from 'html-react-parser';

import {SpellGrid} from './SpellGrid'
import {ClassTokenBar} from "./ClassTokenBar";
import {SpellSearchBar} from "./SpellSearchBar";
import {SERVER_URL} from "../../App";
import {SpellLevelFilter} from "./SpellLevelFilter";

const SpellBookStyle = {
    marginLeft: "90px",
    textAlign: "center"
};

export class SpellBook extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            all_spells: [],
            selected_classes: [],
            selected_levels: [0,1,2,3,4,5,6,7,8,9],
            filter: "",
        };
    }

    applyStringFilter(filterString, spellsToFilter) {
        return spellsToFilter.filter(function (spell) {
            return spell.name.toUpperCase().includes(filterString.toUpperCase());
        });

    }

    applyClassFilter(matching_classes, spellsToFilter) {
        return spellsToFilter.filter(function (spell) {
            let found = false;
            matching_classes.forEach((cls) => {
                if (spell.class.split(", ").includes(cls) === true) {
                    found = true
                }
            });
            return found;
        });
    }

    applyLevelFilter(matching_levels, spellsToFilter) {
        return spellsToFilter.filter(function (spell) {
            let found = false;
            matching_levels.forEach((lvl) => {
                if (spell.level == lvl) {
                    found = true
                }
            });
            return found;
        });
    }


    applyFilters() {
        var filtered = this.state.all_spells;
        filtered = this.applyClassFilter(this.state.selected_classes, filtered);
        filtered = this.applyLevelFilter(this.state.selected_levels, filtered);
        filtered = this.applyStringFilter(this.state.filter, filtered);
        return filtered;
    }

    render() {
        return (
            <div style={SpellBookStyle}>
                <ClassTokenBar SpellBook={this}/>
                <SpellLevelFilter SpellBook={this}/>
                <SpellSearchBar SpellBook={this}/>
                <SpellGrid displayed_spells={this.applyFilters()}/>
                <p style={{"height": "4.0em", "margin": "0px"}}>&nbsp;</p>
            </div>
        );
    }

    componentDidMount() {
        fetch(SERVER_URL + "spells", {
            mode: "cors",
            headers: {"Content-Type": "application/json"},
        })
            .then(response => response.json())
            .then(response => {
                response.forEach((element) => {
                    element.description = parse(element.description);
                    element.higher_level = parse(element.higher_level);
                });
                this.setState({all_spells: response});
            })
            .catch(err => console.error(err));
    }

}
