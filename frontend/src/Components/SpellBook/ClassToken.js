import React from "react";


const BaseClassTokenStyle = {
    background: "#FFFEBD",
    textAlign: "center",
    display: "inline-table",
    margin: "5px",
};
const SelectedClassTokenStyle = {
    boxShadow: "0 0 3px 3px #930C10",
    opacity: "1",
};

const UnselectedClassTokenStyle = {
    opacity: "0.6",
    boxShadow: "0 0 1px 1px #930C10",
};

const TokenImageStyle = {
    margin: "4px"
};


export class ClassToken extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            style: {...BaseClassTokenStyle, ...UnselectedClassTokenStyle},
        };
    }

    classTokenClicked = () => {
        let newState = !this.state.selected;
        let newStyle = newState ? SelectedClassTokenStyle : UnselectedClassTokenStyle;
        newStyle = {...BaseClassTokenStyle, ...newStyle};

        this.props.ClassTokenBar.selectionChanged(this.props.class_name, newState);
        this.setState({selected: newState});
        this.setState({style: newStyle});
    };

    render() {
        return (
            <div style={this.state.style} onClick={() => this.classTokenClicked()} selected={this.state.selected}>
                <img alt={'token'} src={require('../../img/ClassTokens/' + this.props.class_name + '.svg')} width={80}
                     style={TokenImageStyle}/>
                <br/>
                {this.props.class_name}
            </div>
        );
    }
}