import React from "react";

const SpellLevelDivStyle = {
    padding: "10px",
    background: "#F7CE65",
    border: "solid #930C10",
    borderStyle: "none  solid none "
};

const SingleLevelCHeckboxStyle = {
    display: "inline-grid",
    gridTemplateColumns: "auto auto"
};

export class SpellLevelFilter extends React.Component {
    constructor(props) {
        super(props);
        this.selectionChanged = this.selectionChanged.bind(this);

        this.checkboxes = this.props.SpellBook.state.selected_levels.map((i) => {
            return (
                <div style={SingleLevelCHeckboxStyle} key={"Div" + i}>
                    <label key={"Label" + i}>{i}</label>

                    <input name={i} type="checkbox" onChange={this.selectionChanged} key={"Input" + i}
                           defaultChecked={true}/>
                </div>
            )
        });
    }


    render() {
        return (
            <div style={SpellLevelDivStyle}>
                <label> Spell levels: </label>
                {this.checkboxes}
            </div>

        );
    }


    selectionChanged(event) {
        let selected_levels = this.props.SpellBook.state.selected_levels;

        if (event.target.checked) {
            selected_levels.push(event.target.name);
        } else {
            selected_levels.splice(selected_levels.indexOf(event.target.name), 1);
        }

        this.props.SpellBook.setState({selected_levels: selected_levels});

    }
}