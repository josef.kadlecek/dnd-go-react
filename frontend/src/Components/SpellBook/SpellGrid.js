import React from 'react';
import styled from "styled-components";

import {SingleSpell} from "./Spells";

const SpellGridStyle = styled.div`
    background-color: #282c34;
    text-align: center;
    display: grid;
    grid-template-columns: 1fr ; 
    width : 80%;
    margin: auto;
`;

export class SpellGrid extends React.Component {

    render() {

        return (
            <SpellGridStyle>
                {
                    this.props.displayed_spells.map((x, i) => {
                        return (<SingleSpell SpellName={x.name} CastTime={x.casting_time} Components={x.components}
                                             Description={x.description} Range={x.range} School={x.school}
                                             SpellLevel={x.level} User={x.class} Duration={x.duration} Ritual={x.ritual}
                                             Archetype={x.archetype} Source={x.source} Concentration={x.concentration}
                                             Material={x.material ? x.material : "None"} HigherLevel={x.higher_level}
                                             key={x.name}/>)
                    })
                }
            </SpellGridStyle>
        );

    }

}





