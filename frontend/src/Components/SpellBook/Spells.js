import React from 'react';


const SingleSpellStyle = {
    display: "grid",
    padding: "10px",
    textAlign: "center",
    fontSize: "18px",
    color: "#111",

};

const SingleSpellSummaryStyle = {
    backgroundColor: "#F7CE65",
    border: "solid #930C10"
};

const SingleSpellDetailStyle = {
    backgroundColor: "#FFFEBD"
};

export class SingleSpell extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            detailed: false,
        };
    }

    spellClicked(e) {
        this.setState({detailed: !this.state.detailed});
        // Do not prevent default.
    }

    render() {
        if (this.props.HigherLevel) {
            var higher_level = <b>{this.props.HigherLevel}</b>
        }

        if (this.props.Concentration === "yes"){
            var concentration = <b>Requires concentration.</b>
        }

        if (this.props.Ritual === "yes") {
            var ritual = <b>(Ritual)</b>
        }

        this.details = <div style={SingleSpellDetailStyle}>
            <p> Duration: {this.props.Duration}</p>
            <p> Cast time: {this.props.CastTime} | Components: {this.props.Components}  | Materials: {this.props.Material} </p>
            <p>Range: {this.props.Range} </p>
            {concentration}
            {this.props.Description}
            {higher_level}
        </div>;


        return (
            <div style={SingleSpellStyle}>

                <div onClick={() => this.spellClicked()} style={SingleSpellSummaryStyle}>
                    <h3>{this.props.SpellName} {ritual}</h3>
                    <p>{this.props.School} </p>
                </div>

                {!this.state.detailed ? "" : this.details}
            </div>
        );
    }


}