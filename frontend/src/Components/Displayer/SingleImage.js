import React from "react";
import {Group, Image} from "react-konva";

class SingleImage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isDragging: false,
            x: 150,
            y: 250,
            imageHeight: 300,
            imageWidth: 300,
            initiative: 10,
        }
    }


    render() {
        let scale = 1 / (this.state.imageHeight / 400);

        return (
            <Group name={this.props.name} draggable>
                <Image name={this.props.name}
                       image={this.state.image}
                       x={this.state.x}
                       y={80}
                       scaleX={scale} scaleY={scale}
                       onDragStart={() => {
                           this.setState({
                               isDragging: true
                           });
                       }}
                       onDragEnd={e => {
                           this.setState({
                               isDragging: false,
                               x: e.target.x(),
                               y: e.target.y()
                           });
                       }}
                       onClick={() => {
                           this.props.parent.setState({selectedShape: this});
                           this.handleLoad();
                       }}
                />
            </Group>
        )
    }

    componentDidMount() {
        this.image = new window.Image();
        this.image.src = this.props.src;
        this.image.addEventListener('load', this.handleLoad);
        this.setState({
            x: this.state.x + (this.props.imageNumber * 380 + 100)
        });
    }

    handleLoad = () => {
        this.setState({
            image: this.image,
            imageHeight: this.image.height,
            imageWidth: this.image.width,
        });
    };

}

export default SingleImage;
