import React from "react";
import {Layer, Stage} from 'react-konva';
import TransformerComponent from "./Transformer";
import SingleImage from "./SingleImage";
import InitiativeTracker from "./InitiativeTracker/InitiativeTracker";
import {SERVER_URL} from "../../App";

class ImageDisplayer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedShape: "",
            creature_names: ["Specter", "Wight"],
            creatures: [],
        }

    }

    handleStageClick = e => {
        this.setState({
            selectedShape: e.target.name()
        });
    };


    render() {
        return (
            <div>

                <Stage width={window.innerWidth - 250} height={window.innerHeight} onClick={this.handleStageClick}
                       style={{"float": "left"}}>
                    <Layer>
                        {this.state.creatures.map((creature, i) => {
                            return (<SingleImage src={creature.image_url} parent={this} name={creature.name}
                                                 key={creature.name + i} imageNumber={i}/>)
                        })}
                        <TransformerComponent selectedShape={this.state.selectedShape}/>
                    </Layer>
                </Stage>

                <InitiativeTracker creatures={this.state.creatures}/>

            </div>
        );
    }


    componentDidMount() {
        this.state.creature_names.map((creature_name) => {
            fetch(SERVER_URL + "creatures/" + creature_name, {
                mode: "cors",
                headers: {"Content-Type": "application/json"},
            })
                .then(response => response.json())
                .then(response => {
                    let creatures = this.state.creatures;
                    response.initiative = 10;
                    creatures.push(response);
                    this.setState({creatures: creatures});
                })
                .catch(err => console.error(err));
            return null
        });

    }

}

export default ImageDisplayer;
