import React from "react";
import InitiativeComponent from "./InitiativeComponent";
import {SERVER_URL} from "../../../App";

const InitiativeTrackerStyle = {
    fontSize: "30px",
    display: "grid",
    float: "right",
    color: "#930C10",
    width: "220px",
    margin: "10px",
    marginTop: "20px",
    position: "absolute",
    top: "0px",
    right: "0px",
    border: "solid #030303",
    background: "#F7CE65"

};


class InitiativeTracker extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            creatures: this.props.creatures,
            players: [],
            participants: [],
        }
    }

    static getDerivedStateFromProps(props, current_state) {
        if (current_state.participants.length !== props.creatures.length + current_state.players.length) {
            return {
                participants: current_state.players.concat(props.creatures)
            }
        }
        return null
    }

    handleInitiativeClick = (action, name) => {
        var participants = this.state.participants
        let index = participants.findIndex((element) => element.name === name);
        action === "inc" ? participants[index].initiative++ : participants[index].initiative--;

        this.setState({participants: participants});
    };


    sortInitiative = (action, name) => {
        let sorted = this.state.participants.sort((a, b) => b.initiative - a.initiative);
        this.setState({participants: sorted});
    };

    render() {
        return (
            <div style={InitiativeTrackerStyle}>
                {
                    this.state.participants.map((component, i) => {
                        return (
                            <InitiativeComponent name={component.name} initiative={component.initiative} parent={this}
                                                 key={i}/>
                        )
                    })
                }
                <img style={{margin: "auto", padding: "10px"}} alt={'d20'} src={require('../../../img/Icons/d20.png')}
                     width={90} onClick={() => this.sortInitiative()}/>
            </div>
        );
    }


    componentDidMount() {
        //Get playlist list
        fetch(SERVER_URL + "players", {
            mode: "cors",
            headers: {"Content-Type": "application/json"},
        })
            .then(response => response.json())
            .then(response => {
                this.setState({players : response});
                this.setState({participants : response.concat(this.state.creatures)});
            })
            .catch(err => console.error(err));
    }


}

export default InitiativeTracker;
