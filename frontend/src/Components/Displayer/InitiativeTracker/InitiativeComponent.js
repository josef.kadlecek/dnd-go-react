import React from "react";


const InitiativeComponentStyle = {
    width: "190px",
    textAlign: "center",
    border: "solid black",
    margin: "5px",
    padding : "5px",
    "verticalAlign": "middle",
    overflow: "hidden",
    //Unselectable:
    "MozUserSelect" : "-moz-none",
    "KhtmlUserSelect": "none",
    "WebkitUserSelect": "none",
    "OuserSelect": "none",
    "userSelect": "none",
};

class InitiativeComponent extends React.Component {


    render() {
        return (
            <div style={InitiativeComponentStyle}
                 onClick={(e) => this.props.parent.handleInitiativeClick(e.altKey === false ? "inc" : "dec", this.props.name)}>

                <div style={{"float": "left", width : "120px"}}>
                    {this.props.name + ":"}
                </div>

                <div style={{"float": "right"}}>
                    {this.props.initiative}
                </div>

            </div>

        );
    }


}

export default InitiativeComponent;
