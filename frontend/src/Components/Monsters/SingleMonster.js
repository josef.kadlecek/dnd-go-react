import React from "react";
import styled from "styled-components";
import Background from '../../img/Background/pergamen.png';

const SingleMonsterStyle = {
    overflow: "hidden",
    marginRight: "2%",
    padding: "10px",
    width: "700px",
    marginBottom: "2%",
    backgroundImage: `url(${Background})`
};


const GoldenBorder = styled.div`
box-sizing: border-box;
height: 6px;
background-image: url('http://imgsrv.roll20.net:5100/?src=raw.githubusercontent.com/Roll20/roll20-character-sheets/master/5th%20Edition%20OGL%20by%20Roll20/images/Border.png');
border: 1px solid #2f2112;
border-radius: 2px;
// width: 750px;
margin : -11px;
`;

const RedLine = styled.div`
box-sizing: border-box;
border-top: solid 3px transparent;
border-left: solid 315px #a52a2a;
border-bottom: solid 3px #a52a2a;
border-right: solid 315px transparent;
margin: 5px 0px 5px 0px;
`;


//TODO  ImageUrl         string `bson:"image_url"json:"image_url"`
export class SingleMonster extends React.Component {

    render() {
        let creature = this.props.monsterGrid.state.creature;
        let abilityTable = <table style={{width: "500px", textAlign: "center"}}>
            <tbody>
            <tr>
                <th>STR</th>
                <th>DEX</th>
                <th>CON</th>
                <th>INT</th>
                <th>WIS</th>
                <th>CHA</th>
            </tr>
            <tr>
                <td>{creature.STR}</td>
                <td>{creature.DEX}</td>
                <td>{creature.CON}</td>
                <td>{creature.INT}</td>
                <td>{creature.WIS}</td>
                <td>{creature.CHA}</td>
            </tr>
            <tr>
                <td>{creature.STR_mod}</td>
                <td>{creature.DEX_mod}</td>
                <td>{creature.CON_mod}</td>
                <td>{creature.INT_mod}</td>
                <td>{creature.WIS_mod}</td>
                <td>{creature.CHA_mod}</td>
            </tr>
            </tbody>
        </table>;

        let legendaryActions = creature.legendary_actions === undefined || creature.legendary_actions.length === 0 ? <br/> :
            <div>
                <RedLine/>
                <b> Legendary actions </b> <br/>
                {creature.legendary_actions}
            </div>;
        
        
        return <div>

            <div style={SingleMonsterStyle}>
                <GoldenBorder/>

                <h2> {creature.name}</h2>
                {creature.meta}

                <RedLine/>
                AC : {creature.armor_class} <br/>
                HP : {creature.hit_points} <br/>
                Speed : {creature.speed} <br/>
                <RedLine/>
                {abilityTable}
                <RedLine/>
                <b>Saving Throws : </b> {creature.saving_throws || "None"} <br/>
                <b>Skills : </b>{creature.skills}<br/>
                <b>Senses : </b>{creature.senses}<br/>
                <b>Languages : </b>{creature.languages}<br/>
                <b>Challenge : </b>{creature.challenge}<br/>
                <RedLine/>
                {creature.traits}
                <RedLine/>
                {creature.actions}
                {legendaryActions}

                <GoldenBorder/>
            </div>

        </div>

    }
}

export default SingleMonster