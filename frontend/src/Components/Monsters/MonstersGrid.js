import React from "react";
import MonsterFinder from "./MonsterFinder";
import SingleMonster from "./SingleMonster";
import {SERVER_URL} from "../../App";
import parse from "html-react-parser";

const MonstersGridStyle = {
    marginLeft : "92px",
    paddingTop : "20px",
    border : "solid blue",
    overflow: "hidden",
};

//TODO Unify Monster and/or creature
export class MonstersGrid extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            creature: {}
        };
        this.fetchCreature("Wight");
    }

    render() {
        return <div style={MonstersGridStyle}>
            <MonsterFinder monsterGrid={this}/>
            <SingleMonster monsterGrid={this}/>
        </div>
    }

    fetchCreature(creatureName){
        fetch(SERVER_URL + "creatures/" + creatureName, {
            mode: "cors",
            headers: {"Content-Type": "application/json"},
        })
            .then(response => response.json())
            .then(response => {
                response.traits = parse(response.traits);
                response.actions = parse(response.actions);
                response.legendary_actions = parse(response.legendary_actions);
                this.setState({creature: response});

            })
            .catch(err => console.error(err));
    }
}

export default MonstersGrid