import React from "react";

const MonstersFinderStyle = {
    border: "solid red",
    float: "left",
    width: "250px",
    marginRight: "2%",
    background: "#F7CE65",
    textAlign : "center",
};

const MonsterSearchInputStyle = {
    margin: "auto",
    width: "90%",
    height: "40px",
    fontSize: "120%",
    border: "solid #930C10",
    textAlign : "center",
};


export class MonsterFinder extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            searchBarString: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        this.setState({
            searchBarString: event.target.value
        });
        this.props.monsterGrid.fetchCreature(event.target.value);
        event.preventDefault();
    }

    render() {
        return <div style={MonstersFinderStyle}>
            <div style={{border: "solid white", height: "200px", margin: "10px"}}>
                <input type={"text"} name="searchStringInput" autoComplete="off" style={MonsterSearchInputStyle}
                       value={this.state.searchBarString} onChange={this.handleInputChange}
                       onKeyPress={(e) => {
                           e.key === 'Enter' && e.preventDefault();
                       }}/>
            </div>

            <div style={{border: "solid grey", height: "200px", margin: "10px"}}>
                Pinned Monsters
            </div>


        </div>
    }
}

export default MonsterFinder