module backend

go 1.13

require (
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/contrib v0.0.0-20191209060500-d6e26eeaa607
	github.com/gin-gonic/gin v1.5.0
	github.com/stretchr/testify v1.4.0
	go.mongodb.org/mongo-driver v1.3.0
)
