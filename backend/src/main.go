package main

import (
	DB "backend/src/db_handler"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
)

type Env struct {
	db *mongo.Client
}

func main() {
	db := DB.InitDB()
	env := &Env{db}

	// Set the router as the default one shipped with Gin
	router := gin.Default()
	router.Use(cors.Default())
	// Serve frontend static files
	router.Use(static.Serve("/", static.LocalFile("./views", true)))

	api := router.Group("/")
	api.GET("spells", env.GetAllSpells)
	api.GET("classes", GetAllClasses)
	api.GET("playlists", env.GetAllPlaylists)
	api.GET("players", GetPlayers)
	api.GET("creatures/:name", env.GetCreatureByName)

	// Start and run the server
	_ = router.Run(":5000")

}

func GetAllClasses(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, DB.GetAllClasses())
}

func (e *Env) GetAllPlaylists(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, DB.GetAllPlaylists(e.db))
}

func (e *Env) GetCreatureByName(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	creatureName := c.Params.ByName("name")
	c.JSON(http.StatusOK, DB.GetCreatureByName(e.db, creatureName))
}

func GetPlayers(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, DB.GetPlayers())
}

func (e *Env) GetAllSpells(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	allSpells := DB.GetAllSpellsFromDb(e.db)
	c.JSON(http.StatusOK, allSpells)
}
