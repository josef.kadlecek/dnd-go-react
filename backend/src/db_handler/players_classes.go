package db_handler

func GetAllClasses() []string {
	var AllClasses = []string{"Bard", "Cleric", "Druid", "Paladin", "Ranger", "Sorcerer", "Warlock", "Wizard"}
	return AllClasses
}

type Player struct {
	Name       string `json:"name"`
	Initiative int    `json:"initiative"`
}

//TODO Manage by context
func GetPlayers() []Player {
	var AllPlayers = []Player{{"Althea", 10}, {"Bo", 10}, {"Tapo", 10},
		{"Therai", 10}, {"Rhael", 10}}
	return AllPlayers
}
