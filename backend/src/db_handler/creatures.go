package db_handler

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type Creature struct {
	Name             string `bson:"name"json:"name"`
	ImageUrl         string `bson:"image_url"json:"image_url"`
	Meta             string `bson:"meta"json:"meta"`
	ArmorClass       string `bson:"Armor Class"json:"armor_class"`
	HitPoints        string `bson:"Hit Points"json:"hit_points"`
	Speed            string `bson:"Speed"json:"speed"`
	STR              string `bson:"STR"json:"STR"`
	StrMod           string `bson:"STR_mod"json:"STR_mod"`
	DEX              string `bson:"DEX"json:"DEX"`
	DexMod           string `bson:"DEX_mod"json:"DEX_mod"`
	CON              string `bson:"CON"json:"CON"`
	ConMod           string `bson:"CON_mod"json:"CON_mod"`
	INT              string `bson:"INT"json:"INT"`
	IntMod           string `bson:"INT_mod"json:"INT_mod"`
	WIS              string `bson:"WIS"json:"WIS"`
	WisMod           string `bson:"WIS_mod"json:"WIS_mod"`
	CHA              string `bson:"CHA"json:"CHA"`
	ChaMod           string `bson:"CHA_mod"json:"CHA_mod"`
	SavingThrows     string `bson:"Saving Throws"json:"saving_throws"`
	Skills           string `bson:"Skills"json:"skills"`
	Senses           string `bson:"Senses"json:"senses"`
	Languages        string `bson:"Languages"json:"languages"`
	Challenge        string `bson:"Challenge"json:"challenge"`
	Traits           string `bson:"Traits"json:"traits"`
	Actions          string `bson:"Actions"json:"actions"`
	LegendaryActions string `bson:"Legendary Actions"json:"legendary_actions"`
}

func GetCreatureByName(dbClient *mongo.Client, creatureName string) Creature {
	var foundCreature Creature
	collection := dbClient.Database("test").Collection("monsters")

	filter := bson.D{{"name", creatureName}}
	_ = collection.FindOne(context.TODO(), filter).Decode(&foundCreature)

	return foundCreature
}
