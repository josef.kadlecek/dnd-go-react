package db_handler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetCreatureByName(t *testing.T) {
	db := InitDB()
	defer closeDB(db)

	creature := GetCreatureByName(db, "Skeleton")
	assert.Equal(t, "Skeleton", creature.Name)
	assert.Equal(t, "13 (Armor Scraps)", creature.ArmorClass)
}
