package db_handler

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
)

func InitDB() *mongo.Client {
	mongoUrl := os.Getenv("MONGO_URL")
	if mongoUrl == "" {
		log.Print("Mongo url not set. Using 'mongodb'.")
		mongoUrl = "mongodb"
	}

	clientOptions := options.Client().ApplyURI("mongodb://" + mongoUrl + ":27017")
	dbClient, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = dbClient.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal("Unable to reach DB", err)
	}

	return dbClient
}
