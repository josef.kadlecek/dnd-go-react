package db_handler

import (
	"context"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"testing"
)

func closeDB(db *mongo.Client) {
	err := db.Disconnect(context.TODO())
	if err != nil {
		log.Fatal("ERROR while closing DB Connection.")
	}
}

func TestDBConnection(t *testing.T) {
	dbClient := InitDB()

	err := dbClient.Ping(context.TODO(), nil)
	assert.Nil(t, err)

	err = dbClient.Disconnect(context.TODO())
	assert.Nil(t, err)

	err = dbClient.Ping(context.TODO(), nil)
	assert.NotNil(t, err)
}
