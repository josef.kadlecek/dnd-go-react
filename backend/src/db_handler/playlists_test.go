package db_handler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetAllPlaylists(t *testing.T) {
	db := InitDB()
	defer closeDB(db)

	allPlaylists := GetAllPlaylists(db)
	assert.True(t, len(allPlaylists) == 6, "Number of categories matches.")
}
