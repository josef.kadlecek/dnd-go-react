package db_handler

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Playlist struct {
	SpellName     string `bson:"name"json:"name"`
	Description   string `bson:"description"json:"description"`
	Source        string `bson:"source"json:"source"`
	Range         string `bson:"range"json:"range"`
	Components    string `bson:"components"json:"components"`
	Material      string `bson:"material"json:"material"`
	Ritual        string `bson:"ritual"json:"ritual"`
	Duration      string `bson:"duration"json:"duration"`
	Concentration string `bson:"concentration"json:"concentration"`
	CastTime      string `bson:"casting_time"json:"casting_time"`
	SpellLevel    string `bson:"level"json:"level"`
	School        string `bson:"school"json:"school"`
	User          string `bson:"class"json:"class"`
	Archetype     string `bson:"archetype"json:"archetype"`
	HigherLevel   string `bson:"higher_level"json:"higher_level"`
}

func GetAllSpellsFromDb(dbClient *mongo.Client) []Playlist {
	collection := dbClient.Database("test").Collection("spells")

	cur, err := collection.Find(context.TODO(), bson.D{{}}, options.Find())
	if err != nil {
		log.Fatal(err)
	}

	var allSpells []Playlist
	for cur.Next(context.TODO()) {
		var elem Playlist
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		allSpells = append(allSpells, elem)
	}

	return allSpells
}
