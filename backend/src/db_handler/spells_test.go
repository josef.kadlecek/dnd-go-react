package db_handler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func contains(spells []Playlist, sampleSpellName string) bool {
	for _, singleSpll := range spells {
		if singleSpll.SpellName == sampleSpellName {
			return true
		}
	}
	return false
}

func TestGetAllSpellsFromDb(t *testing.T) {
	db := InitDB()
	defer closeDB(db)

	allSpells := GetAllSpellsFromDb(db)
	assert.True(t, len(allSpells) > 100)
	assert.True(t, contains(allSpells, "Abi-Dalzim's Horrid Wilting"))
	assert.True(t, contains(allSpells, "Misty Step"))
	assert.True(t, contains(allSpells, "Vicious Mockery"))
}
