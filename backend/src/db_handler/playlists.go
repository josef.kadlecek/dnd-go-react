package db_handler

import (
	"context"
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"
)

type AccessTokenData struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	Scope       string `json:"scope"`
	ExpiresIn   int    `json:"expires_in"`
}

type ImgUrlData struct {
	Height string `json:"height"`
	Url    string `json:"url"`
	Width  string `json:"width"`
}

type playlist struct {
	PlaylistID  string `json:"playlistID"`
	Category    string `json:"category"`
	PlaylistUrl string `json:"playlist_url"`
	ImgUrl      string `json:"img_url"`
}

type playlistCategory struct {
	Category  string     `json:"category"`
	Playlists []playlist `json:"playlists"`
}

func readTokenFromResponse(resp *http.Response) string {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	bodyData := new(AccessTokenData)
	err = json.Unmarshal(body, bodyData)
	if err != nil {
		log.Fatal(err)
	}

	return bodyData.AccessToken
}

func getAccessToken() string {
	requestBody := strings.NewReader(`grant_type=client_credentials`)

	client := http.Client{
		Timeout: 10 * time.Second,
	}

	request, err := http.NewRequest("POST", "https://accounts.spotify.com/api/token", requestBody)
	if err != nil {
		log.Fatal(err)
	}

	request.Header.Set("Authorization", spotifySecret)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	return readTokenFromResponse(resp)
}

func (pl *playlist) fillImgUrl(accessToken string, wg *sync.WaitGroup) {
	defer wg.Done()

	const playlistImagePrefix = "https://api.spotify.com/v1/playlists/"
	req, err := http.NewRequest("GET", playlistImagePrefix+pl.PlaylistID+"/images", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+accessToken)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	//TODO chage unmarshaler to decoder https://stackoverflow.com/questions/15672556/handling-json-post-request-in-go
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	//TOD Sometimes (usually when refreshing a lot) return body is empty.Solve.
	bodyData := new([]ImgUrlData)
	err = json.Unmarshal(body, bodyData)

	if err != nil {
		pl.ImgUrl = "Unable to fetch data!"
		return
	}

	pl.ImgUrl = (*bodyData)[0].Url
}

func (pl *playlist) fillPlaylistUrl() {
	const playlistUrlPrefix = "https://open.spotify.com/embed/playlist/"
	pl.PlaylistUrl = playlistUrlPrefix + pl.PlaylistID
}

func getAllPlaylistsFromDB(dbClient *mongo.Client) []playlistCategory {
	collection := dbClient.Database("test").Collection("playlists")

	cur, err := collection.Find(context.TODO(), bson.D{{}}, options.Find())
	if err != nil {
		log.Fatal(err)
	}

	var allPlaylists []playlistCategory
	for cur.Next(context.TODO()) {
		var elem playlistCategory
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		allPlaylists = append(allPlaylists, elem)
	}

	return allPlaylists

}

func fillPlaylistDetails(allPlaylists *[]playlistCategory) {
	accessToken := getAccessToken()
	var wg sync.WaitGroup

	//Fil data for every playlist in every category.
	for category := range *allPlaylists {
		for i := range (*allPlaylists)[category].Playlists {
			wg.Add(1)
			go (*allPlaylists)[category].Playlists[i].fillImgUrl(accessToken, &wg)
			(*allPlaylists)[category].Playlists[i].fillPlaylistUrl()
		}
	}
	wg.Wait()

}

func GetAllPlaylists(dbClient *mongo.Client) []playlistCategory {
	AllPlaylists := getAllPlaylistsFromDB(dbClient)
	fillPlaylistDetails(&AllPlaylists)
	return AllPlaylists
}

const spotifySecret = "Basic OTUxMTk5ZGM1MDIzNDg0Y2ExZDM5NTI3ZjYzMjJmMjM6Zjg3OTQxZjk5Yzk4NDY1NmE4ZDM0NzFlOWNiNTEwNTQ="
